library(C50)

get_c50_model<-function(train, ignore_features){


    features <- names(train)
    ignore_features<-c(ignore_features, "normExpected", "discExpected")
    features<-features[!(features %in% ignore_features)]

    # LOG data (to better reflect MAE), and normalize it
    train$normExpected <- log1p(train$Expected)
    tmean <- mean(train$normExpected, na.rm=T)
    tsd <- sd(train$normExpected, na.rm = T)
    train$normExpected <- train$normExpected-tmean
    train$normExpected <- train$normExpected/tsd

    low<-min(train$normExpected)
    high<-max(train$normExpected)

    # Bucket possible results
    steps<-50
    breaks<- 0:(steps)
    step<-(high-low)/steps
    breaks<-breaks*step
    breaks<-breaks+low
    train$discExpected <- factor(  cut(train$normExpected, breaks, labels = 1:steps  ) )

    c50_model<-C5.0( x=train[,features], y=train$discExpected, trials = 3, rules= FALSE)
    list(c50_model, breaks, tsd, tmean)
}


c50_predict<-function(c50_model_data, test){

    c50_model<-c50_model_data[[1]]
    breaks<-c50_model_data[[2]]
    tsd<-c50_model_data[[3]]
    tmean<-c50_model_data[[4]]

    predictions<-predict(c50_model,test)

    predictions<-as.numeric(as.character(predictions))
    predictions <- (breaks[predictions] + breaks[predictions+1])/2

    predictions <- expm1(  ( predictions*tsd) + tmean  )
}

c50_bagged_models<-function(train, ignore_features, nbags=100, rseed=13){

    set.seed(rseed)

    indexes<-round(runif(nrow(train), 1, nbags))
    sets<-split(train, indexes)

    c50_models<-NULL
    for(i in 1:nbags){
        print(paste("Creating model for bag #", i) )

#        if(i==27)
#            print(sets[[i]]$Id)
#        m<-get_c50_model(train[starts[i]:ends[i], ], ignore_features)

        # idiot giving "c50 code called exit with value 1"; don't know why...
        m<-get_c50_model(sets[[i]], ignore_features)
        if( m[[1]]$tree == ""){
            print(paste("ERROR: ignoring model for bag #", i))
        }else{
            c50_models<-rbind(c50_models, m)
        }
    }
    c50_models
}

c50_bagged_predict<-function(c50_models, test){

    predictions<-NULL
    for(i in 1:nrow(c50_models)){
        print(paste("Calculating predictions with model #", i))
        p<-c50_predict(c50_models[i,], test)
        predictions<-cbind(predictions, p)
    }

    rowMeans(predictions)
}
