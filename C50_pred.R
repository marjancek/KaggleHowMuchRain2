source("./get_data.R")
source("./c50.R")

c50_results_file<-"c50_bagged100_trials3.csv"
rseed<-13

train<-get_train(rseed)
ignore_features<-c("Expected","Id", "logExpected")

print("C5.0 creating model...")
c50_model_data<-c50_bagged_models(train, ignore_features, 200, rseed)

rm(train)
gc()

test<-get_test(rseed)
c50_results <- data.frame(ID=test$Id)
c50_results$Expected <- c50_bagged_predict(c50_model_data, test)

write.csv(c50_results, c50_results_file, row.names=FALSE, quote=FALSE)
