# README #

marjancek's attempts to [Kaggle's](https://www.kaggle.com) "[How much did it Rain II](https://www.kaggle.com/c/how-much-did-it-rain-ii)"


This is a set of source code files aimed to study and solve a Kaggle's challenge.
Most it is R code, with some Python for pre-processing.

Feel free to take a look, learn from my mistakes, and leave any constructive comments you may have. 