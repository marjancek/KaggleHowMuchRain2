library(data.table)
source("config.R")


mae<-function(prediction, real){
    mean(abs(prediction-real),na.rm=T)
}


printlog <- function(...) {
    print(paste(Sys.time(), ":", ..., sep=" "))
}

create_timed_log<-function(){
    start<-Sys.time()
    log_msg<-function(...){
        elapsed <- round(as.numeric(Sys.time()) - as.numeric(start))
        elapsed<-format(elapsed, width=10, flag="0")
        print(paste(elapsed , ":", ..., sep=" "))
    }
}


get_train<-function(ranseed=13){

    printlog("Reading train data...")
    train<-read.csv( train_filename )

    printlog("Preprocess train data...")
    train<-preprocess(train)
}

get_train_and_validation<-function(ranseed=13, factor=.80){

    train <- get_train(ranseed)

    train_size <- nrow(train)
    train_ids <- sample( train_size, train_size*factor)
    val_ids <- setdiff(1:train_size, train_ids)

    training <- train[train_ids, ]
    validation <- train[val_ids, ]

    list(training, validation)
}

preprocess<-function(train){
    printlog("Feature and row engineering...")

    if(!is.null(train$Expected)){
        train <- train[train$Expected<68,]
        train <- train[train$Expected>0,]
        train <- train[!is.na(train$Expected),]
        train$logExpected <- log1p(train$Expected)
    }

    train$naCounts <- rowSums(is.na(train))
    train$meanRef <- rowMeans((train[, c("Ref_15", "Ref_30", "Ref_45", "Ref_60")]), na.rm=T)
    train$sumRef  <- rowSums((train[,  c("Ref_15", "Ref_30", "Ref_45", "Ref_60")]), na.rm=T)
    train$meanZdr <- rowMeans((train[, c("Zdr_15", "Zdr_30", "Zdr_45", "Zdr_60")]), na.rm=T)

    train$RefComposite <- rowMeans((train[, c("RefComposite_15", "RefComposite_30","RefComposite_45", "RefComposite_60")]), na.rm=T)

    #train$meanKdp <- rowMeans((train[, c("Kdp_15", "Kdp_30", "Kdp_45", "Kdp_60",)]), na.rm=T)
    #train$RhoHV <- rowMeans((train[, c("RhoHV_15", "RhoHV_30", "RhoHV_45", "RhoHV_60",)]), na.rm=T)

    train$logRef <- log(train$meanRef)
    train[is.nan(train$logRef),]$logRef <- NA
    train$logZdr <- log(train$meanZdr)
    train[is.nan(train$logZdr),]$logZdr <- NA

    train$Ref_5x5_90th <- rowMeans((train[, c("Ref_5x5_90th_15", "Ref_5x5_90th_30","Ref_5x5_90th_45", "Ref_5x5_90th_60")]), na.rm=T)
    train$Ref_5x5_90_by_Ref <- train$Ref_5x5_90th * train$meanRef
    train$logR_5x5_90th <- log(train$Ref_5x5_90th)
    train[is.nan(train$logR_5x5_90th),]$logR_5x5_90th <- NA
    train$logRefComposite <- log(train$RefComposite)
    train[is.nan(train$logRefComposite),]$logRefComposite <- NA
    train$Ref_on_RefCom <- train$meanRef / train$RefComposite
    train
}


get_test<-function(ranseed=13){
    printlog("Reading test data...")
    test <- read.csv(test_filename)

    printlog("Preprocess test data...")
    train<-preprocess(test)
}
